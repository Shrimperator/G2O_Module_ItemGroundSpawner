// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online ItemGroundSpawn Scripts     -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

addEvent("onInitItemSpawners");

// ------------------------------------------------------------------- //

addEventHandler("onPlayerDropItem", function(pid, itemGround)
{
	ItemGroundSpawner.onPlayerDropItem(pid, itemGround);
});

// ------------------------------------------------------------------- //

addEventHandler("onPlayerTakeItem", function(pid, itemGround)
{
	ItemGroundSpawner.onPlayerTakeItem(pid, itemGround);
});

// ------------------------------------------------------------------- //

addEventHandler("onInit", function()
{
    ItemGroundSpawner.init();
});

// ------------------------------------------------------------------- //

local nextCheckSpawnsTick = getTickCount();
local checkSpawnsTickInterval = 1000;

addEventHandler("onTick", function()
{
    local tick = getTickCount();
    if (tick >= nextCheckSpawnsTick)
    {
        ItemGroundSpawner.checkSpawns();
        nextCheckSpawnsTick = tick + checkSpawnsTickInterval;
    }
});

// ------------------------------------------------------------------- //

TIMESPAN_SHORTEST <- 100;       // -- 100ms
TIMESPAN_SHORTER <- 1000;       // -- 1s
TIMESPAN_SHORT <- 10000;        // -- 10s
TIMESPAN_DEFAULT <- 60000;      // -- 1m
TIMESPAN_LONG <- 600000;        // -- 10m
TIMESPAN_LONGER <- 3600000;     // -- 1h
TIMESPAN_LONGEST <- 36000000;   // -- 10h

// ------------------------------------------------------------------- //

class SpawnableItem
{
    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- Getters -- //

    function getItem() {return m_Item;}
    function getAmount() {return m_Amount;}
    function getPosition() {return m_Pos;}
    function getWorld() {return m_World;}
    function getSpawnInterval() {return m_SpawnInterval;}
    function getNextSpawnTick() {return m_NextSpawnTick;}

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    constructor(item, amount, x, y, z, world, spawnInterval)
    {
        m_Item = item;
        m_Amount = amount;
        m_Pos = {["x"] = x, ["y"] = y, ["z"] = z};
        m_World = world;
        m_SpawnInterval = spawnInterval;
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    m_ID = -1;
    m_Item = null;
    m_Amount = null;
    m_Pos = null;
    m_World = null;
    m_SpawnInterval = null;
    m_NextSpawnTick = null;
}

// ------------------------------------------------------------------- //

class ItemGroundSpawner
{
    static function init()
    {
		local startTick = getTickCount();
        print("Initializing itemspawners...");

        callEvent("onInitItemSpawners");

		local deltaMS = getTickCount() - startTick;
        if (m_SpawnableItems.len() == 1)
    		print("Initialized " + m_SpawnableItems.len() + " itemspawner, took: " + deltaMS + "ms");
    	else
    		print("Initialized " + m_SpawnableItems.len() + " itemspawners, took: " + deltaMS + "ms");
    	print("-========================================-");
    }

    // ------------------------------------------------------------------- //

    static function add(item, amount, x, y, z, world, spawnInterval = TIMESPAN_DEFAULT)
    {
		local spawnableItem = SpawnableItem(item, amount, x, y, z, world, spawnInterval);
        spawnableItem.m_ID = m_SpawnableItems.len();
        m_SpawnableItems.append(spawnableItem);
        spawnItem(spawnableItem);
    }

    // ------------------------------------------------------------------- //

    static function checkSpawns()
    {
        local tick = getTickCount();
        if (m_SpawnSchedule.len() > 0)
        {
            local spawnableItem = m_SpawnSchedule.top();
            if (spawnableItem.getNextSpawnTick() <= tick)
            {
                spawnItem(spawnableItem);
                unregisterSpawn(spawnableItem);

                checkSpawns();
            }
        }
    }

    // ------------------------------------------------------------------- //

    static function spawnItem(spawnableItem)
    {
        local pos = spawnableItem.getPosition();
        local itemGround = ItemsGround.spawn(spawnableItem.getItem(), spawnableItem.getAmount(), pos.x, pos.y, pos.z, spawnableItem.getWorld());

        m_SpawnedItems[itemGround.id] <- spawnableItem;
    }

    // ------------------------------------------------------------------- //

    static function unregisterSpawn(spawnableItem)
    {
        foreach (i, otherItem in m_SpawnSchedule)
        {
            if (otherItem == spawnableItem)
            {
                m_SpawnSchedule.remove(i);
                break;
            }
        }
    }

    // ------------------------------------------------------------------- //

    static function registerSpawn(spawnableItem)
    {
        spawnableItem.m_NextSpawnTick = getTickCount() + spawnableItem.getSpawnInterval();
        unregisterSpawn(spawnableItem);

        // -- Sort array by nextSpawnTick, so only the item that is closest to being spawned has to be checked -- //
        if (m_SpawnSchedule.len() == 0)
            m_SpawnSchedule.append(spawnableItem)
        else
        {
            for (local i = m_SpawnSchedule.len() - 1; i >= 0; i--)
            {
                if (m_SpawnSchedule[i].getNextSpawnTick() >= spawnableItem.getNextSpawnTick())
                {
                    m_SpawnSchedule.insert(i + 1, spawnableItem);
                    break;
                }
                else if (i == 0)
                    m_SpawnSchedule.insert(0, spawnableItem);
            }
        }
    }

    // ------------------------------------------------------------------- //

    static function onPlayerTakeItem(pid, itemGround)
    {
        if (itemGround.id in m_ItemsGround)
            delete m_ItemsGround[itemGround.id];

        // -- Restart spawner -- //
        if (itemGround.id in m_SpawnedItems)
        {
            registerSpawn(m_SpawnedItems[itemGround.id]);
            delete m_SpawnedItems[itemGround.id];
        }
    }

    // ------------------------------------------------------------------- //

    static function onPlayerDropItem(pid, itemGround)
    {
        m_ItemsGround[itemGround.id] <- itemGround;
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    static m_ItemsGround = {}

    static m_SpawnableItems = [];
    static m_SpawnSchedule = [];
    static m_SpawnedItems = {};
}

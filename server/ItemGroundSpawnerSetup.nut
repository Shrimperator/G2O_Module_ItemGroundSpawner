// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online ItemGroundSpawn Scripts     -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

addEventHandler("onInitItemSpawners", function()
{
    // -- Items in newworld harbor (0|0|0) -- //
    ItemGroundSpawner.add(Items.id("ITFO_APPLE"), 1, 0, 0, 0, "NEWWORLD\\NEWWORLD.ZEN", TIMESPAN_SHORTEST);
    ItemGroundSpawner.add(Items.id("ITFO_BREAD"), 2, 100, 0, 0, "NEWWORLD\\NEWWORLD.ZEN", TIMESPAN_SHORT);
    ItemGroundSpawner.add(Items.id("ITFO_BEER"), 3, 200, 0, 0, "NEWWORLD\\NEWWORLD.ZEN", TIMESPAN_DEFAULT);
    ItemGroundSpawner.add(Items.id("ITFO_HONEY"), 4, 300, 0, 0, "NEWWORLD\\NEWWORLD.ZEN", 10000);
});
